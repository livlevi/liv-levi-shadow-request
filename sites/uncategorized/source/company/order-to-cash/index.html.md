---
layout: markdown_page
title: "Order to Cash"
description: "GitLab's Order to Cash systems and processes"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

This is cross-functional page meant which is meant to be the source of truth for our Order to Cash systems.

## Snowflake Data Warehouse and dbt (data build tool)  

We extract data from the Order to Cash systems towards Snowflake and use dbt to transform the data into data models for reporting and analysis.

### ERDs (Entity Relationship Diagrams)

These ERDs illustrate how we model data from the Order to Cash Systems in the Snowflake Enterprise Dimensional Model.

#### Sales Funnel Dimensional Model ERD (Built using Salesforce data)

<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embeddedchart/b09f9e0a-e695-4cba-882d-981a93216293" id="7Da6Neo1dhab"></iframe></div>

#### Service Ping Dimensional Model ERD (Built with CustomerDot, Version App, Zuora, and Salesforce Data)

<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embedded/3a42e56a-028e-45d7-b2ca-5ef489bafd32" id="Z1Mr2IgZz268"></iframe></div>

#### Common Subscription Dimensional Model ERD (Built using Salesforce and Zuora Data)

<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embedded/7becf6b4-98d8-4e49-8467-764a3296622d" id="T3MrtOiP96Ov"></iframe></div>

#### ARR (Annual Recurring Revenue) Dimensional Model ERD (Built using Salesforce and Zuora Data)

<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embedded/998dbbae-f04e-4310-9d85-0c360a40a018" id="f5MrUO8fEC1Y"></iframe></div>

### dbt Data Lineage Diagrams

These data lineage diagrams illustrate how the data from critical Order to Cash source tables flow through the Snowflake data models.

#### Zuora Data Lineages

- [Zuora Account Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.zuora_account_source?g_v=1&g_i=zuora_account_source%2B)
- [Zuora Rate Plan Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.zuora_rate_plan_source?g_v=1&g_i=zuora_rate_plan_source%2B)
- [Zuora Rate Plan Charge Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.zuora_rate_plan_charge_source?g_v=1&g_i=zuora_rate_plan_charge_source%2B)
- [Zuora Subscription Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.zuora_subscription_source?g_v=1&g_i=zuora_subscription_source%2B)
- [Zuora Contact Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.zuora_contact_source?g_v=1&g_i=zuora_contact_source%2B)

#### Salesforce Data Lineages

- [Salesforce Opportunity Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.sfdc_opportunity_source?g_v=1&g_i=sfdc_opportunity_source%2B)
- [Salesforce Account Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.sfdc_account_source?g_v=1&g_i=sfdc_account_source%2B)
- [Salesforce Zuora Quote Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.sfdc_zqu_quote_source?g_v=1&g_i=sfdc_zqu_quote_source%2B)
- [Salesforce Zuora Quote Rate Plan Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.sfdc_zqu_quote_rate_plan_source?g_v=1&g_i=%2Bsfdc_zqu_quote_rate_plan_source%2B)
- [Salesforce Zuora Quote Rate Plan Charge Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.sfdc_zqu_quote_rate_plan_charge_source?g_v=1&g_i=sfdc_zqu_quote_rate_plan_charge_source%2B)

#### CustomerDot Data Lineages

- [CustomerDot License Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.customers_db_licenses_source?g_v=1&g_i=customers_db_licenses_source%2B)
- [CustomersDot Orders Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.customers_db_orders_source?g_v=1&g_i=customers_db_orders_source%2B)
- [CustomersDot Lead Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.customers_db_leads_source?g_v=1&g_i=customers_db_leads_source%2B)
- [CustomersDot Trial History Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.customers_db_trial_histories_source?g_v=1&g_i=customers_db_trial_histories_source%2B)
- [CustomerDot Versions Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.customers_db_versions_source?g_v=1&g_i=customers_db_versions_source%2B)
- [CustomerDot Customers Source](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.customers_db_customers_source?g_v=1&g_i=customers_db_customers_source%2B)

### dbt Data Dictionaries

These data dictionaries provide definitions for the Order to Cash fields used in the Snwoflake data model.

#### Zuora

`Coming Soon`

#### Salesforce

`Coming Soon`

#### CustomerDot

`Coming Soon`

### Business Insights and Analysis

Our Data Catalog provides access to Analytics Hubs, Data Guides, ERDs, and Analytics projects relating to the Order to Cash business processes. 

- Lead to Cash Data Catalog
- Product Release to Adoption Data Catalog
